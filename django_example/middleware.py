import logging

from django.http import HttpResponse
from django.urls import resolve
from django.utils.deprecation import MiddlewareMixin


class ZeroDivisionExceptionMiddleware(MiddlewareMixin):

    def process_exception(self, request, exception):
        try:
            # поднимаем ошибку снова но если ошибка деления на ноль
            raise exception
        except ZeroDivisionError as exception_message:
            # То обрабатываем эту ошибку, пишем в логи и кидает обычный респонс
            logging.error('Urgent - error with zero division')
            return HttpResponse('Sorry, just error with zero division')


class ViewValidationMiddleware(MiddlewareMixin):

    def process_view(self, request, view_func, view_args, view_kwargs):
        # resolve функция для вытягивание информации по пути который в запросе
        resolved_path_info = resolve(request.path_info)

        # Если страница отображение студентов то передадим доп параметр во view
        if resolved_path_info.url_name == 'students_list':
            some_variable = 10
            return view_func(request, some_variable)

        # А всех остальных будем отображать как обычно
        return view_func(request, *view_args, **view_kwargs)


class LogRequestResponseMiddleware(MiddlewareMixin):

    def process_request(self, request):
        # процесинг запроса
        resolved_path_info = resolve(request.path_info)
        logging.info(
            'Request %s View name %s route %s',
            request, resolved_path_info.view_name, resolved_path_info.route)

    def process_response(self, request, response):
        # процесинг ответа
        logging.info('For request %s response %s', request, response)
        return response